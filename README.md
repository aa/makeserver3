Makeserver3 is a relaunch of the makeserver, written specifically to take advantage of python 3's new asyncronous IO features. In a nutshell, makeserver is about creating a scaffolding server that allows dynamic websites to be developed using a makefile.

Requires

* python 3.5
* aiohttp


Install
--------------------------

  pip install aiohttp
  python setup.py install


To do
--------------------------
* (websockets-based) view to monitor output of make command
* add --js option to mixin custom javascript plugins (a la greasemonkey)
* add --saveable option to mixin saveable javascript

makewiki (aka cookbook)
--------------------------
Curated set of deployable makefiles

For instance to convert AVI to webm

